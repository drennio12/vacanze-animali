<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via
 * web, è anche possibile copiare questo file in «wp-config.php» e
 * riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Prefisso Tabella
 * * Chiavi Segrete
 * * ABSPATH
 *
 * È possibile trovare ultetriori informazioni visitando la pagina del Codex:
 *
 * @link https://codex.wordpress.org/it:Modificare_wp-config.php
 *
 * È possibile ottenere le impostazioni per MySQL dal proprio fornitore di hosting.
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define('DB_NAME', 'vacanze-animali-test');

/** Nome utente del database MySQL */
define('DB_USER', 'ennio-stage');

/** Password del database MySQL */
define('DB_PASSWORD', 'Demoted99');

/** Hostname MySQL  */
define('DB_HOST', 'localhost');

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define('DB_CHARSET', 'utf8mb4');

/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'h;%6Y;.Aht5nTk=# ^L!X+1!|mCMYnV[ m^68m{WvU(cpD4xH6^qJC*TVI}](u5V');
define('SECURE_AUTH_KEY',  'Nj#Ilk6|z_6Q5_;bfl4$@)Ob2fsg=6O4C<1Qn<5*jDo8b<kM:},ulYvhbXSVg~M/');
define('LOGGED_IN_KEY',    ':6iSq,xl~/wPB^2_$xwYNz){8jcQLatkN1O~`OCsT-K{t[O^KaY8Dtr>/0nnl5`r');
define('NONCE_KEY',        '1d;-o$._sO e}|id&8#3^zys0yF.mFmJH9<Zea }sn1kK,Mt=_qe~?.Tfi;</YXU');
define('AUTH_SALT',        '169w0.!9kY10E4@vdl>e3WO8TeLQs$3;c!5#5s$3%4Yct#~OjEFiFHq0[]!a@%)?');
define('SECURE_AUTH_SALT', '.EpYTM=.0h7}ud982YB$uM#Io/EyUJlnoQ[!x&C$%oc{g)y]Dn7,Yvg.S,b26J*{');
define('LOGGED_IN_SALT',   'iK>K>lB4VNENm,Q0/jK&&j9lB*f tzep.H:7j@rl+-!kDlE#$x3j^^$)=3zq<Xto');
define('NONCE_SALT',       ' #T0[^Adz6lV<YL/]5ng6Qu? bwq;;-!$:o QN#RKLm!km7a:!2!WLpD,U=`$^t-');

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix  = 'wp_';

/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');