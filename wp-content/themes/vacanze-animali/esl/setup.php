<?php

//add backend support for Google analytics
require('analytics.php');

//add support for custom login page
require('custom-login.php');

//add support for custom login page
require('header-logo.php');

//add support for social links
require('social.php');

//add custom header logo settings to customizer
add_action( 'customize_register', 'esl_customize' );

function esl_customize( $wp_customize ) {
	$wp_customize->add_panel( 'esl_panel', array(
		'title' => 'Eurosoftlab',
		'description' => 'Impostazioni Eurosoftlab',
		'priority' => 200,
	) );
}

/**
 * Add an element to a customizer panel
 *
 * @param [type] $wp_customize	customizer object
 * @param [type] $section		name of the customizer section
 * @param [type] $settingName	name of the setting (in database)
 * @param [type] $label			text for the label
 * @param [type] $priority		order in the customizer panel
 * @return void
 */
function AddElement($wp_customize, $section, $settingName, $label, $priority){
	$wp_customize->add_setting($settingName, array(
		'default'		=> '',
		'capability'	=> 'edit_theme_options',
		'type'			=> 'option'
	));

	$wp_customize->add_control($settingName,
		array(
			'label'		=> __( $label, 'eurosoftlab' ),
			'type'		=> 'text',
			'section'	=> $section,
			'settings'	=> $settingName,
			'priority'	=> $priority
		)
	);
}

function AddSection($wp_customize, $section, $title, $description, $priority){
	$wp_customize->add_section( $section, 
		array(
			'title' => $title,
			'description' => $description,
			'priority' => $priority,
			'panel' => 'esl_panel'
		)
	);
}