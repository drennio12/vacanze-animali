<?php

/**
 * Return file name for header logo
 *
 * @return string
 */
function esl_head_logo(){
	return get_option('header_logo');
}

/**
 * Return alt text for header logo
 *
 * @return string
 */
function esl_head_logo_alt_text(){
	return get_option('header_logo_alt_text');
}

/**
 * Return custom css class for header logo
 *
 * @return string
 */
function esl_head_logo_class(){
	return get_option('header_logo_class');
}

//add custom header logo settings to customizer
add_action( 'customize_register', 'theme_customize_header_logo' );

function theme_customize_header_logo( $wp_customize ) {
	$section = 'esl_header_logo';

	AddSection($wp_customize, $section, 'Header', 'Consente di personalizzare il logo che compare nell\'header', 20);

	//image file (from media library) for logo
	$wp_customize->add_setting('header_logo', array(
		'default'		=> '',
		'capability'	=> 'edit_theme_options',
		'type'			=> 'option'
	));

	$wp_customize->add_control( new WP_Customize_Image_Control(
			$wp_customize,
			'header_logo',
			array(
				'label'		=> __( 'Header logo', 'eurosoftlab' ),
				'section'	=> $section,
				'settings'	=> 'header_logo',
				'priority'	=> 1
			)
		)
	);

	//alt text for header logo
	AddElement($wp_customize, $section, 'header_logo_alt_text', 'Testo alternativo', 2);

	//alt text for header logo
	AddElement($wp_customize, $section, 'header_logo_class', 'Classe css', 3);
}