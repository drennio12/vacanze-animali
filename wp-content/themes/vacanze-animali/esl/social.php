<?php

/**
 * Return facebook profile url
 *
 * @return string
 */
function esl_social_facebook(){
	return get_option('esl_profile_facebook_link');
}

/**
 * Return linkedin profile url
 *
 * @return string
 */
function esl_social_linkedin(){
	return get_option('esl_profile_linkedin_link');
}

/**
 * Return twitter profile url
 *
 * @return string
 */
function esl_social_twitter(){
	return get_option('esl_profile_twitter_link');
}

/**
 * Return instagram profile url
 *
 * @return string
 */
function esl_social_instagram(){
	return get_option('esl_profile_instagram_link');
}

//add custom header logo settings to customizer
add_action( 'customize_register', 'theme_customize_social_links' );

function theme_customize_social_links( $wp_customize ) {
	$section = 'esl_social_links';

	AddSection($wp_customize, $section, 'Social links', 'Consente di specificare gli indirizzi dei profili social', 40);

	//facebook profile url
	AddElement($wp_customize, $section, 'esl_profile_facebook_link', 'Profilo Facebook', 1);

	//linkedin profile url
	AddElement($wp_customize, $section, 'esl_profile_linkedin_link', 'Profilo Linkedin', 2);

	//instagram profile url
	AddElement($wp_customize, $section, 'esl_profile_instagram_link', 'Profilo Instagram', 3);

	//twitter profile url
	AddElement($wp_customize, $section, 'esl_profile_twitter_link', 'Profilo Twitter', 4);
}
