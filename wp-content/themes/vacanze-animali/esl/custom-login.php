
<?php

//Login logo customization
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo() { ?>
	<style type="text/css">
		.login h1 a {
			background-image: url('<?php echo get_option('login_logo'); ?>')!important;
			padding-bottom: 30px;
			background-size: auto;
			width: auto;
		}
	</style>
<?php
}

add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url() {
	return 'https://www.eurosoftlab.com';
}

add_filter( 'login_headertitle', 'my_login_logo_url_title' );

function my_login_logo_url_title() {
	return 'Web site by Eurosoftlab';
}

//add custom login logo settings to customizer
add_action( 'customize_register', 'theme_customize_login_logo' );

function theme_customize_login_logo( $wp_customize ) {
	$section = 'esl_login_logo';

	AddSection($wp_customize, $section, 'Login', 'Consente di personalizzare il logo che compare nella finestra di login', 30);

	$wp_customize->add_setting('login_logo', array(
		'default'        => '',
		'capability'     => 'edit_theme_options',
		'type'           => 'option'
	));

	$wp_customize->add_control( new WP_Customize_Image_Control(
			$wp_customize,
			'login_logo',
			array(
				'label'      => __( 'Login logo', 'nessuno-escluso' ),
				'section'    => $section,
				'settings'   => 'login_logo',
			)
		)
	);
}

?>