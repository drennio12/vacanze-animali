<?php
/**
 * vacanze con animali functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package vacanze_con_animali
 */

require_once('inc/support.php');
require_once('inc/custom-post.php');
require_once('inc/custom-taxonomy.php');
if (is_admin()) require_once('inc/custom-metabox.php');
require_once('esl/setup.php');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

ThemeSetup();

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
