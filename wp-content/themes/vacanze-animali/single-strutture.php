<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package vacanze_con_animali
 */

require_once ('inc/utility.php');
require ('inc/single-elements.php');

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php $post_id = get_the_ID(); ?>

					<article id="post-<?php echo $post_id; ?>" <?php post_class(); ?>>
						<header class="structure-header">
							<?php
								the_title( '<h1 class="structure-title">', '</h1>' );
							?>
						</header><!-- .structure-header -->

						<div class="structure-content">
							<?php
								PostImage($post_id, 'single-post-featured-image');

								the_content( sprintf(
									the_title( '<span class="screen-reader-text">"', '"</span>', false )
								) );
								DivGallery( $post_id );
							?>
						</div><!-- .structure-content -->

						<footer class="structure-footer">
							<?php vacanze_animali_entry_footer(); ?>
						</footer><!-- .structure-footer -->
					</article><!-- #post-## -->

				<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>
			</div><!-- .container -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
?>