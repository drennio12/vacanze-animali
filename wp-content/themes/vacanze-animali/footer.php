<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package vacanze_con_animali
 */

?>

		  	</div>
		<div id="push"></div>
  	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info container">
			<div class="footer-business-name span12">
				<div class="vcard" itemscope itemtype="http://schema.org/Corporation">
					<div class="footer-address span12">
						<img id="logofooter" src="<?php echo wp_get_upload_dir()['url']; ?>/logo-vacanza-con-animali.png" alt="logo vacanze con animali" style="height:150px; width: 150px;"/>
						<h1>Vacanze con gli animali</h1>
						<h4>è un'idea di <span class="fn  org" itemprop="name">Sunrise s.a.s. </span>
						<br />
						<span class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span class="street-address" itemprop="streetAddress">Via Porto Palos, 6</span> - <span class="postal-code" itemprop="postalCode">47922</span> <span class="locality" itemprop="addressLocality">Rimini</span> - <span class="country-name" itemprop="addressCountry">RN</span></span>
					</div>
					<div class="footer-contact span12">
						tel. <a href="tel:+390541732230"><span class="tel" itemprop="telephone">0541 732230</span></a> - email <a href="mailto:claudio@eurosoftlab.com"><span class="email" itemprop="email">info@vacanzaconanimali.com</span></a>
					</div>
					<div class="footer-contact span12">
						<a href="<?php echo get_bloginfo('url'); ?>/aggiungi-la-tua-struttura/" rel="nofollow">Aggiungi la tua struttura</a> -
						<a href="<?php echo get_bloginfo('url'); ?>/aggiungi-la-tua-azienda/" rel="nofollow">Aggiungi la tua azienda</a>
					</div>
					<div class="footer-privacy span12">
						<a href="<?php echo get_bloginfo('url'); ?>/privacy" rel="nofollow">informativa privacy</a> - <a href="<?php echo get_bloginfo('url'); ?>/informativa-cookies" rel="nofollow">cookie policy</a> - <a href="<?php echo get_bloginfo('url'); ?>/condizioni-generali" rel="nofollow">condizioni generali</a>
					</div>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
	<div class="back-top" style="bottom: -100px;">
		<span class="glyphicon glyphicon-chevron-up"></span>
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
