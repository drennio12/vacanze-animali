<?php
/**
 * The template for advanced search.
 *
 * @package vacanze_con_animali
 */
get_header(); ?>

<?php //do_action('__before_main_wrapper'); ##hook of the header with get_header ?>
<div id="main-wrapper" class="<?php echo implode(' ', apply_filters('tc_main_wrapper_classes' , array('container'))) ?>">
    <div class="container" role="main">
        <div id="content" class="">
    
            <?php include_once('inc/ricerca-box.php') ?>

        </div><!--.article-container -->
    </div><!-- .container role: main -->
</div><!--#main-wrapper"-->

<?php
get_sidebar();
get_footer();
