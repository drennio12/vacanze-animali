<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package vacanze_con_animali
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

<?php
	$analytics = get_option('analytics_code');

	if ($analytics != '') {
		add_google_analytics($analytics);
	}
?>

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'vacanze-animali' ); ?></a>
	<header id="masthead" class="site-header" role="banner" style="background-image: url(<?php echo( get_header_image() ); ?>);background-attachment:fixed;background-size:contain;background-repeat:no-repeat;}">
		<div class="site-branding container">
			<div class="site-branding row">
				<div class="header-logo col-xs-12 col-sm-1">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<img id="header-img" src="<?php echo wp_get_upload_dir()['url']; ?>/logo-vacanza-con-animali.png" alt="logo vacanze con animali"/>
					</a>
				</div>
				<div class="header-text col-xs-12 col-sm-11">
					<div class="site-title col-xs-12 col-sm-12">
						<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
					</div>
					<div class="site-description col-xs-12 col-sm-12">
						<?php
						$description = get_bloginfo( 'description', 'display' );
						if ( $description || is_customize_preview() ) : ?>
							<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
						<?php
						endif; ?>
					</div>
				</div>
			</div>
		</div><!-- .site-branding -->
			<nav id="site-navigation" class="main-navigation" role="navigation">
				<button id="navbar-toggle" type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="main-menu container collapse navbar-collapse navbar-ex1-collapse">
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
				</div><!-- .main-menu -->
			</nav><!-- #site-navigation -->

	</header><!-- #masthead -->

	<div id="content" class="site-content">
