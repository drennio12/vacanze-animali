<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package vacanze_con_animali
 */

require ('inc/utility.php');

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container">
				<div class="col-md-1"></div>
				<div class="single-container col-md-10">
					<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="single-header col-md-12">
							<div class="single-title col-md-8">
								<?php
								the_title( '<h1 class="single-title">', '</h1>' );
							?>
							</div>
							<div class="single-animali col-md-4">
								<h2 class="animali-acpt">Animali accettati:</h2>
								<?php 
								$id = get_the_ID();
								//$text = the_terms($id,'animali');
								$terms = get_terms( 'animali' );
								if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
									$count= count($terms);
									$i=0;
    								foreach ( $terms as $term ) {
										$i++;
										$term_list .= '<h2 class="animali-title">' . $term->name;
										if ( $count != $i ) {
											$term_list .= ',';
										}
										else{
											$term_list.= '</h2>';
										}
									}
									echo $term_list;
								}
							
								?>
							</div>
						</header>
						<!-- .single-header -->
						<div class="single-content col-md-12">
							<div class="single-slide col-md-12">
								<div class="slide-img col-md-4">
									<?php SinglePostImage(get_the_ID(), 'single-post-featured-image'); ?>
								</div>
								<div class="slide-img col-md-4">
									<?php SinglePostImage(get_the_ID(), 'single-post-featured-image'); ?>
								</div>
								<div class="slide-img col-md-4">
									<?php SinglePostImage(get_the_ID(), 'single-post-featured-image'); ?>
								</div>
						</div>
						<div class=" single-desc col-md-12">
								<?php	the_content( sprintf(
								the_title( '<span class="screen-reader-text">"', '"</span>', false )
								) );
								?>
							</div>
						<!-- .single-content -->

						<footer class="single-footer col-md-12">
								<div class="col-md-9">
								</div>
								<div class="disp-button col-md-3">
									<a href="#myModal1" role="button" class="btn btn-large btn-primary" data-toggle="modal">RICHIEDI DISPONIBILIT&Agrave;</a>
								</div>
							<?php vacanze_animali_entry_footer(); ?>
						</footer>
						<!-- .single-footer -->
				</div>
				<!--.single-container -->
				</article>
				<!-- #post-## -->
				<div id="myModal1" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">Compila il modulo per inviare la richiesta</h4>
							</div>
							<div class="modal-body">
								<?php echo do_shortcode('[contact-form-7 id="278" title="Richiesta di disponibilità"]'); ?>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default btnsubmit" data-dismiss="modal">Chiudi</button>
							</div>
						</div>
					</div>
				</div>
				<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>
			</div>
			<div class="col-md-1"></div>
			<!-- .container -->
		</main>
		<!-- #main -->
	</div>
	<!-- #primary -->

	<?php
get_sidebar();
get_footer();
?>