<?php

function DivGallery($post_id){
	$ids = array(); 
	$gallery = get_post_meta( $post_id, '_custom_field_galleria', 1 );

	if ( ( $gallery != '' ) && ( count($gallery) ) > 0 ) {
		foreach ( $gallery as $image ){
			$ids[] = array_search($image, $gallery);
		}
		?>

		<div class="gallery panel">
			<?php
			$gallery_shortcode = '[gallery size="thumbnail" link="file" ids="' . implode(",", $ids) . '"]';
			echo slb_activate( do_shortcode( $gallery_shortcode ) );
			?>
		</div>

			<div class="social-links col-xs-12">
				<?php echo do_shortcode('[ssba]'); ?>
			</div>

		<?php
	}
}

?>