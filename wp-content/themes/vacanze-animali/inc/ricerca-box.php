<?php
/*
* Research box management
*/

require_once('utility.php');

?>
<form class="row-fluid rForm"
		<?php
			$site_url = network_site_url( '/' );
		?>
		action="<?php echo $site_url ?>risultati" method="POST">
			<div class="research-box">
				<h1 class="her-center">Ricerca Avanzata</h1>
				<div class="category-col col-xs-12">
					<span class="chRec"></span>
						<h2>Categoria</h2>
						<?php
							$sql= "SELECT DISTINCT va_terms.term_id, name, taxonomy, va_termmeta.meta_value FROM va_terms INNER JOIN va_term_taxonomy ON va_terms.term_id=va_term_taxonomy.term_id INNER JOIN va_termmeta ON va_terms.term_id = va_termmeta.term_id WHERE taxonomy='category' AND va_termmeta.meta_key = '_categorie_immagine' AND va_term_taxonomy.count > 0 ORDER BY name ASC";
						    $checkboxes = $wpdb->get_results($sql, ARRAY_A);

						    foreach ($checkboxes as $checkbox):
								?>
								<span class="search-item">
									<div class="col-xs-4 col-md-3">
										<input type="checkbox" class="btn btn-success ch" name="<?php echo $checkbox['taxonomy'] ?>[]" id="_<?php echo $checkbox['name'] ?>" value="<?php echo $checkbox['term_id'] ?>">
										<label class="label ch2" for="_<?php echo $checkbox['name'] ?>"><img src="<?php echo $checkbox['meta_value'] ?>" class="advanced-research-category"/></label>
									</div>
								</span>
						    <?php endforeach; ?>
					<br><br>
				</div>
				<div class="locality-col col-xs-12">
					<h2>Localit&agrave;</h2>
					<?php
						$sql= "SELECT va_terms.term_id, name, taxonomy FROM va_terms INNER JOIN va_term_taxonomy ON va_terms.term_id=va_term_taxonomy.term_id WHERE (taxonomy='localita' AND va_term_taxonomy.count!=0) ORDER BY name";
					    $checkboxes = $wpdb->get_results($sql, ARRAY_A);

					    foreach ($checkboxes as $checkbox): ?>
					      	<span class="search-item">
								<div class="col-xs-4 col-md-3"> <?php
									echo '<input type="checkbox" class="btn btn-success" name="'.$checkbox['taxonomy'].'[]" id="_'.$checkbox['name'].'" value="'.$checkbox['term_id'].'">';
									echo '<label class="label" for="_'.$checkbox['name'].'">'.$checkbox['name'].'</label>'; ?>
								</div>
							</span>
						<?php endforeach; ?>
				</div>
				<div class="trattamenti-col col-xs-12">
					<h2>Trattamenti</h2>
					<?php
						$sql= "SELECT va_terms.term_id, name, taxonomy FROM va_terms INNER JOIN va_term_taxonomy ON va_terms.term_id=va_term_taxonomy.term_id WHERE taxonomy='trattamenti'";
					    $checkboxes = $wpdb->get_results($sql, ARRAY_A);
					    foreach ($checkboxes as $checkbox):
					      	?> <span class="search-item">';
								<div class="col-xs-4 col-md-3">';
									<?php
										echo '<input type="checkbox" class="btn btn-success" name="'.$checkbox['taxonomy'].'[]" id="_'.$checkbox['name'].'" value="'.$checkbox['term_id'].'">';
										echo '<label class="label" for="_'.$checkbox['name'].'">'.$checkbox['name'].'</label>';
									?>
								</div>
							</span>
							<?php
					    endforeach;
					?>
				</div>
				<div class="services-col col-xs-12">
					<h2>Servizi</h2>
					<?php
						$sql= "SELECT va_terms.term_id, name, taxonomy FROM va_terms INNER JOIN va_term_taxonomy ON va_terms.term_id=va_term_taxonomy.term_id WHERE taxonomy='servizi' AND va_term_taxonomy.count!=0 ORDER BY name";
					    $checkboxes = $wpdb->get_results($sql, ARRAY_A);

					    foreach ($checkboxes as $checkbox):
							?>
					      	<span class="search-item">
								<div class="col-xs-4 col-md-3">
									<?php
										echo '<input type="checkbox" class="btn btn-success" name="'.$checkbox['taxonomy'].'[]" id="_'.$checkbox['name'].'" value="'.$checkbox['term_id'].'">';
										echo '<label class="label" for="_'.$checkbox['name'].'">'.$checkbox['name'].'</label>';
									?>
								</div>
							</span>
							<?php
					    endforeach;
					?>
				</div>
				<!--<div class="offers-col">
				</div>-->
				<div class="submit col-xs-12">
					<br>
					<input type="submit" class="research-submit" value="Cerca">
				</div>
			</div>
		</form>
<?php

?>