<?php
/*
* Custom metabox management
*/

require_once( 'custom-metabox-support.php' );

// Associazione delle funzioni alle azioni di wordpress per la gestione del metabox
add_action( 'cmb2_admin_init', 'categorie_meta_box_taxonomy' );
add_action( 'cmb2_admin_init', 'add_metabox_indirizzo' );
add_action( 'cmb2_admin_init', 'add_metabox_galleria' );
add_action( 'cmb2_admin_init', 'add_metabox_evidenza' );
add_action( 'cmb2_admin_init', 'add_metabox_contatti' );
//add_action( 'cmb2_admin_init', 'add_metabox_mappaiframe' );

function categorie_meta_box_taxonomy() {
	$prefix = '_categorie_';

	$cmb_term = new_cmb2_box( array(
		'id'               => $prefix . 'edit',
		'title'            => __( 'Immagine Categoria', 'cmb2' ),
		'object_types'     => array( 'term' ),
		'taxonomies'       => array( 'category', 'categorie-partner' )
	) );

	AddFieldToMetabox($cmb_term, $prefix, 'Immagine Categoria', 'immagine', 'file');
}

function add_metabox_galleria() {
	$prefix = '_custom';

	$cmb = new_cmb2_box(
		array(
			'id'           => $prefix . 'galleria',
			'title'        => __( 'Galleria', 'cmb2' ),
			'object_types' => array('strutture'),
			'show_names'   => true
		)
	);

	AddFieldToMetabox($cmb, $prefix, 'Galleria', 'galleria', 'file_list', false, array( 100, 100 ));
}


// Funzione per la definizione di un metabox e indicazione della funzione da richiamare
function add_metabox_indirizzo(){
	$prefix = '_custom_';

	$cmb = new_cmb2_box(
		array(
			'id'           => $prefix . 'indirizzo',
			'title'        => __( 'Indirizzo', 'cmb2' ),
			'object_types' => array('strutture', 'partner' ),
			'show_names'   => true
		)
	);

	AddFieldToMetabox($cmb, $prefix, 'Indirizzo', 'indirizzo', 'text');
	AddFieldToMetabox($cmb, $prefix, 'CAP', 'cap', 'Text');
	AddFieldToMetabox($cmb, $prefix, 'Località', 'localita', 'text');
	AddFieldToMetabox($cmb, $prefix, 'Provincia', 'provincia', 'text');

}

function add_metabox_contatti() {
	$prefix = '_esl';

	$cmb = new_cmb2_box(
		array(
			'id'			=> $prefix . 'contatti',
			'title'			=> __( 'Contatti e posizione', 'cmb2' ),
			'object_types'	=> array('strutture', 'partner'),
			'show_names'	=> true
		)
	);

	AddFieldToMetabox($cmb, '', 'Priorità', 'priorita', 'text_small');

	$group_field_id = $cmb->add_field( array(
		'id'			=> $prefix . '_indirizzo',
		'type'			=> 'group',
		'description'	=> __( 'Indirizzo', 'cmb2' ),
		'repeatable'	=> false,
		'options'		=> array( 'group_title'   => __( 'Indirizzo', 'cmb2' ) ),
	) );

	// Id's for group's fields only need to be unique for the group. Prefix is not needed.
	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Via',
		'id'	=> 'via',
		'type'	=> 'text'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'CAP',
		'id'	=> 'cap',
		'type'	=> 'text'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Citta',
		'id'	=> 'citta',
		'type'	=> 'text'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Prov',
		'id'	=> 'provincia',
		'type'	=> 'text_small'
	) );

	$group_field_id = $cmb->add_field( array(
		'id'			=> $prefix . '_consigliato',
		'type'			=> 'group',
		'description'	=> __( 'Dati per mostrare la struttura prima delle altre nelle ricerche', 'cmb2' ),
		'repeatable'	=> false,
		'options'		=> array( 'group_title'   => __( 'Consigliato', 'cmb2' ) ),
	) );

	// Id's for group's fields only need to be unique for the group. Prefix is not needed.
	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Consigliato',
		'id'	=> 'consigliato',
		'type'	=> 'checkbox'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Dal',
		'id'	=> 'dal',
		'type'	=> 'text_date'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Al',
		'id'	=> 'al',
		'type'	=> 'text_date'
	) );

	$group_field_id = $cmb->add_field( array(
		'id'			=> $prefix . '_contatti',
		'type'			=> 'group',
		'description'	=> __( 'Dati di contatto', 'cmb2' ),
		'repeatable'	=> false,
		'options'		=> array( 'group_title'   => __( 'Contatti', 'cmb2' ) ),
	) );

	// Id's for group's fields only need to be unique for the group. Prefix is not needed.
	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Telefono',
		'id'	=> 'telefono',
		'type'	=> 'text_small'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Email',
		'id'	=> 'email',
		'type'	=> 'text_email'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Dal',
		'id'	=> 'dal',
		'type'	=> 'text_date'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Al',
		'id'	=> 'al',
		'type'	=> 'text_date'
	) );

	//add Gmap and position
	$group_field_id = $cmb->add_field( array(
		'id'			=> $prefix . '_posizione',
		'type'			=> 'group',
		'description'	=> __( 'Posizione', 'cmb2' ),
		'repeatable'	=> false,
		'options'		=> array( 'group_title'   => __( 'Posizione', 'cmb2' ) ),
	) );

	// Id's for group's fields only need to be unique for the group. Prefix is not needed.
	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Mappa',
		'id'	=> 'mappa',
		'type'	=> 'gmap',
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Indirizzo Gmap',
		'id'	=> 'indirizzo_gmap',
		'type'	=> 'text',
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Latitudine',
		'id'	=> 'latitudine',
		'type'	=> 'text',
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Longitudine',
		'id'	=> 'longitudine',
		'type'	=> 'text',
	) );
}
/*function add_metabox_mappaiframe(){
	$prefix = '_custom_';

	$cmb = new_cmb2_box(
		array(
			'id'           => $prefix . 'mappa',
			'title'        => __( 'Iframe mappa', 'cmb2' ),
			'object_types' => array('page'),
			'show_names'   => true
		)
	);

	AddFieldToMetabox($cmb, $prefix, 'Iframe mappa', 'iframe-mappa', 'text');
	AddFieldToMetabox($cmb, $prefix, 'CAP', 'cap', 'Text');
	AddFieldToMetabox($cmb, $prefix, 'Località', 'localita', 'text');
	AddFieldToMetabox($cmb, $prefix, 'Provincia', 'provincia', 'text');

}*/
?>
