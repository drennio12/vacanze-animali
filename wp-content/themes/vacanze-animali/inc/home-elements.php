<?php

function HomeStructure(){ ?>
	<div class="article col-xs-6 col-md-4">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="home-entry-header">
				<a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>" rel="bookmark">
					<?php
						PrintPostThumbnail('medium');
					?>
				</a>
				<?php
					the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>' );
				?>
			</header>
			<?php
				generateLocalityDiv();
			?>
			<div class="home-entry-content">
				<?php
					the_content( sprintf(
						/* translators: %s: Name of current post. */
						wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'vacanze-animali' ), array( 'span' => array( 'class' => array() ) ) ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					) );

					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'vacanze-animali' ),
						'after'  => '</div>',
					) );
				?>
			</div>

			<div class="entry-footer">
				<?php HomeElementFooter('struttura'); ?>

			</div>
		</article>
	</div>
	<?php
}

function HomePartner(){ ?>
		<?php the_category();?>
		<div class="article col-xs-6 col-md-4">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="home-entry-header">
					<a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>" rel="bookmark">
						<?php the_post_thumbnail('medium') ?>
					</a>
					<?php
					the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>' );
				?>
				</header>
				<!-- .home-entry-header -->
				<div class="home-entry-content">
					<?php 
					the_content( sprintf(
						/* translators: %s: Name of current post. */
						wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'vacanze-animali' ), array( 'span' => array( 'class' => array() ) ) ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					) );

					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'vacanze-animali' ),
						'after'  => '</div>',
					) );
				?>
				</div>
				<!-- .home-entry-content -->

				<div class="entry-footer">
					<?php HomeElementFooter('partner'); ?>
				</div>
				<!-- .entry-footer -->
			</article>
			<!-- #post-## -->
		</div>
		<!-- article -->
		<?php
}
function HomeMapItem(){	?>
			<?php  ?>
			<div class="article col-xs-6 col-md-6">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="home-map-header">
						<a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>" rel="bookmark">
							<?php the_post_thumbnail('thumbnail') ?>
						</a>
						<?php
					the_title('<h3 id="article-title" class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h3>' );
				?>
					</header>
					<!-- .home-entry-header -->
					<div class="home-map-content">
						<?php
						// Commentato perchè veniva visualizzato tutto il content nel ciclo delle strutture
					//the_content( sprintf(
						/* translators: %s: Name of current post. */
					//	wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'vacanze-animali' ), array( 'span' => array( 'class' => array() ) ) ),
					//	the_title( '<span class="screen-reader-text">"', '"</span>', false )
					//) );

					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'vacanze-animali' ),
						'after'  => '</div>',
					) );
				?>
					</div>
					<!-- .home-entry-content -->

					<!--<div class="map-footer">
				<?php// HomeElementFooter('partner'); ?>
			</div>-->
					<!-- .entry-footer -->
				</article>
				<!-- #post-## -->
			</div>
			<!-- article -->
			<?php	
}
function HomeArticle(){	?>
				<?php 
$categories = get_categories('taxonomy=categorie-partner&post_type=partner');
foreach ($categories as $category){ //Ciclo per prendere i nomi delle categories i linkarli alle rispettive pagine
	$getlink = get_category_link($category->cat_ID);
	$catID = get_category($category->cat_ID);
	$terms = get_the_terms( get_the_ID(), 'categorie-partner' ); 						 
	$category_links = array();
}
$post = get_the_ID();
$taxonomy = 'categorie-partner';
//get_the_terms($post, $taxonomy); 
?>
				<div class="article col-xs-6 col-md-6">
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="home-article-header">
							<a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>" rel="bookmark">
								<?php the_post_thumbnail('thumbnail') ?>
							</a>
							<?php
			the_title('<h3 id="article-title" class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h3>' );?>
						</header>
					</article>
				</div>
				<!-- article -->
				<?php
}

function ArchiveArticles(){	?>
					<?php 
$categories = get_categories('taxonomy=categorie-partner&post_type=partner');
foreach ($categories as $category){ //Ciclo per prendere i nomi delle categories i linkarli alle rispettive pagine
	$getlink = get_category_link($category->cat_ID);
	$catID = get_category($category->cat_ID);
	$terms = get_the_terms( get_the_ID(), 'categorie-partner' ); 						 
	$category_links = array();
}
$post = get_the_ID();
$taxonomy = 'categorie-partner';
//get_the_terms($post, $taxonomy); 
?>
					<div class="article-archive col-xs-12 col-md-12">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<header class="home-article-header col-md-3">
								<a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>" rel="bookmark">
									<?php the_post_thumbnail(array( 200, 200)) ?>
								</a>
								<?php //the_title('<h3 id="article-title" class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h3>' );?>
							</header>
							<div class="col-md-9 article-right-container">
								<div style="" class="col-md-12 archive-article-title">
									<?php the_title('<h3 class="entry-title-archive"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h3>' );?>
								</div>
								<div style="" class="col-md-12 archive-article-description">
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam efficitur augue nulla, nec vulputate enim consectetur ac.
										Sed consequat metus sed venenatis porttitor. Integer neque neque, convallis at euismod nec, egestas ac velit. In
										nec turpis tempus, viverra est sed, molestie sapien. Suspendisse potenti. Morbi vehicula viverra arcu, in blandit
										orci auctor eu. Integer nulla neque, tincidunt eu lacus vitae, egestas ultrices dui. Nam blandit dignissim faucibus.
										Maecenas vel ex laoreet arcu sodales aliquet. Aliquam porttitor turpis ac nisl vehicula posuere.
									</p>
								</div>
								<?php 
			$taxo = 'localita';
			$categorie = get_categories('taxonomy=localita&post_type=partner');
			foreach ($categorie as $categoria){
				$terms = get_the_terms( get_the_ID(), $taxo);
			}       
			if ( $terms && ! is_wp_error( $terms ) ) : 
    			foreach ( $terms as $term ) {
        			$localita = $term->name;
					}
			endif;
			//FINE PRIMO LOOP
			//INIZIO SECONDO
			$tax = 'posizione';
			$categorie = get_categories('taxonomy=posizione&post_type=partner');
			foreach ($categorie as $categoria){
				$terms2 = get_the_terms( get_the_ID(), $tax);
			}       
			if ( $terms2 && ! is_wp_error( $terms ) ) : 
    			foreach ( $terms2 as $term2 ) {
        			$posizione = $term2->name;
					}
			endif;
			//FINE SECONDO LOOP
			//INIZIO TERZO LOOP
			$tax1 = 'animali';
			$categorie1 = get_categories('taxonomy=animali&post_type=partner');
			foreach ($categorie1 as $categoria1){
				$terms1 = get_the_terms( get_the_ID(), $tax1);
			}       
			if ( $terms1 && ! is_wp_error( $terms1 ) ) : 
    			foreach ( $terms1 as $term1 ) {
					$animali[] = $term1->name;								 
				$aniamliactp = join( ", ", $animali );
			}
			endif;
			?>
								<div class="info-article-container col-md-12">
									<div class="col-md-4 info-article">
										<h2 class="info-article-text">Provincia:</h2>
										<h3 class="locality">&#32;&#32;
											<?php echo $localita; ?>
										</h3>
									</div>
									<div class="col-md-4 info-article">
										<h2 class="info-article-text">Posizione:</h2>
										<h3 class="locality">&#32;&#32;
											<?php echo $posizione; ?>
										</h3>
									</div>
									<div class="col-md-4 info-article">
										<h2 class="info-article-text">Animali Accettati:</h2>
										<h3 class="locality">&#32;&#32;
											<?php echo $aniamliactp; ?>
										</h3>
									</div>
								</div>
							</div>
						</article>
					</div>
					<!-- article -->
					<?php
}?>