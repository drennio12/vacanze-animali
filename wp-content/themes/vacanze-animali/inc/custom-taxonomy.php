<?php
/*
* custom taxonomy management
*/
add_action('init', 'servizi_strutture_init');
add_action('init', 'servizi_partner_init');
add_action('init', 'tipi_strutture_init');
//add_action('init', 'categorie_strutture_init');
add_action('init', 'categorie_partner_init');
add_action('init', 'localita_init');
add_action('init', 'posizione_init');
add_action('init', 'animali_init');


function servizi_strutture_init() {
	// create a new taxonomy
	$labels = array(
		'name' => __('Servizi'),
		'view_item' => __('Visualizza servizio'),
		'edit_item' => __('Modifica servizio'),
		'add_new_item' => __('Aggiungi nuovo servizio')
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'rewrite' => array('slug' => 'servizi'),
		'hierarchical' => true
	);

	register_taxonomy(
		'servizi',
		array('strutture'),
		$args
	);
}

function servizi_partner_init() {
	// create a new taxonomy
	$labels = array(
		'name' => __('Servizi'),
		'view_item' => __('Visualizza servizio'),
		'edit_item' => __('Modifica servizio'),
		'add_new_item' => __('Aggiungi nuovo servizio')
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'rewrite' => array('slug' => 'servizi-partner'),
		'hierarchical' => true
	);

	register_taxonomy(
		'servizi-partner',
		array('partner'),
		$args
	);
}

function tipi_strutture_init() {
	// create a new taxonomy
	$labels = array(
		'name' => __('Tipologie'),
		'view_item' => __('Visualizza tipologia'),
		'edit_item' => __('Modifica tipologia'),
		'add_new_item' => __('Aggiungi nuova tipologia')
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'rewrite' => array('slug' => 'tipo-strutture'),
		'hierarchical' => true
	);

	register_taxonomy(
		'tipi-strutture',
		array('strutture'),
		$args
	);
}

function categorie_strutture_init() {
	// create a new taxonomy
	$labels = array(
		'name' => __('Categorie'),
		'view_item' => __('Visualizza categoria'),
		'edit_item' => __('Modifica categoria'),
		'add_new_item' => __('Aggiungi nuova categoria')
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'rewrite' => array('slug' => 'categorie-strutture'),
		'hierarchical' => true
	);

	register_taxonomy(
		'categorie-strutture',
		array('strutture'),
		$args
	);
}

function categorie_partner_init() {
	// create a new taxonomy
	$labels = array(
		'name' => __('Categorie'),
		'view_item' => __('Visualizza categoria'),
		'edit_item' => __('Modifica categoria'),
		'add_new_item' => __('Aggiungi nuova categoria')
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'rewrite' => array('slug' => 'categorie-partner'),
		'hierarchical' => true
	);

	register_taxonomy(
		'categorie-partner',
		array('partner'),
		$args
	);
}

function localita_init() {
	// create a new taxonomy
		$labels = array(
		'name' => __('Localit&agrave;'),
		'view_item' => __('Visualizza Localit&agrave;'),
		'edit_item' => __('Modifica Localit&agrave;'),
		'add_new_item' => __('Aggiungi nuova Localit&agrave;')
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'rewrite' => array('slug' => 'localita'),
		'hierarchical' => true
	);

	register_taxonomy(
		'localita',
		array('strutture', 'partner'),
		$args
	);
}

function posizione_init() {
	// create a new taxonomy
		$labels = array(
		'name' => __('Posizione'),
		'view_item' => __('Visualizza Posizione'),
		'edit_item' => __('Modifica Posizione'),
		'add_new_item' => __('Aggiungi nuova Posizione')
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'rewrite' => array('slug' => 'posizione'),
		'hierarchical' => true
	);

	register_taxonomy(
		'posizione',
		array('strutture', 'partner'),
		$args
	);
}

function animali_init() {
	// create a new taxonomy
		$labels = array(
		'name' => __('Animali accettati'),
		'view_item' => __('Visualizza Animali accettati'),
		'edit_item' => __('Modifica Animali accettati'),
		'add_new_item' => __('Aggiungi nuovi Animali accettati')
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'rewrite' => array('slug' => 'animali'),
		'hierarchical' => true
	);

	register_taxonomy(
		'animali',
		array('strutture', 'partner'),
		$args
	);
}