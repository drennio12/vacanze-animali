<?php
/*
* custom post management
*/

add_action('init', 'create_struttura_post_type');
add_action('init', 'create_partner_post_type');
add_action('init', 'remove_standard_post_type');

function create_struttura_post_type() {
	register_post_type('strutture',
		array(
			'labels' => array(
				'name' => __('Strutture'),
				'singular_name' => __('Struttura'),
				'view_item' => __('Visualizza struttura'),
				'edit_item' => __('Modifica struttura'),
                'add_new_item' => __('Aggiungi nuova struttura')
			    ),
            'public' => true,
            'has_archive' => true,
            'show_in_nav_menus ' => true,
            'menu_position' => 5,
            'taxonomies' =>  array('category'),
            'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
            'rewrite' => array('slug' => 'struttura')
		    )
	);
}

function create_partner_post_type() {
	register_post_type('partner',
		array(
			'labels' => array(
				'name' => __('Partner'),
				'singular_name' => __('Partner'),
				'view_item' => __('Visualizza partner'),
				'edit_item' => __('Modifica partner'),
                'add_new_item' => __('Aggiungi nuovo partner')
			    ),
            'public' => true,
            'has_archive' => true,
            'menu_position' => 6,
            'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
            'rewrite' => array('slug' => 'partner')
            )
	);
}

function remove_standard_post_type() {
	register_post_type('post',
		array(
            'public' => false,
            'has_archive' => false,
    		)
	);
}

?>
