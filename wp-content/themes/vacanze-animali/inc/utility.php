<?php

//returns the post featured image
function PostImage($ID, $customClass){
        if (has_post_thumbnail( $ID ) ) :
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $ID ), 'single-post-thumbnail' );?>
            <img src='<?php echo $image[0]; ?>' class='<?php echo $customClass; ?>' />	
            </div>
            <?php
        endif;
}

//Custom function for returning single posts featured image ENNIO
function SinglePostImage($ID, $customClass){
	if (has_post_thumbnail( $ID ) ) :
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $ID ), array('220','220') );?>
		<img src='<?php echo $image[0]; ?>' class='<?php echo $customClass; ?>' />
		<?php
	endif;
}

//returns the post category image
function CategoryImage($category, $imageClass){
    $image = get_metadata('term', $category->term_id, '_categorie_immagine', true);

    if ($image != ''){
        ?> <img src="<?php echo $image; ?>" class="<?php echo $imageClass ?>" /> <?php
    } else {
        ?> <div class="category"></div> <?php
    }
}

//print the post thumbnail, if post doesn't have thumnail print the default image
function PrintPostThumbnail($size, $class = ''){
    if ( has_post_thumbnail() ) { 
        the_post_thumbnail($size);
    } else {
        DefaultImage($class);
    }
}

//prints the img tag for default image
function DefaultImage($class = ''){
	?>
		<img src="<?php echo wp_get_upload_dir()['url']; ?>/logo-vacanza-con-animali.png" alt="vacanze con animali" class="<?php $class ?>"/>
	<?php
}

function generateLocalityDiv(){
	$terms = get_the_terms( get_the_id(), 'localita' );
	if ($terms){
		$term = array_pop($terms);
		echo "<div class=\"locality\"><a href='".get_term_link($term->slug, 'localita')."'>".$term->name."</a></div>";
	} else {
		echo "<div class=\"locality\"></div>";
	}
}

function generateLocality(){
	$terms = get_the_terms(get_the_id(), 'localita','parent');

	if ($terms && ! is_wp_error($terms)){ 
		foreach ($terms as $term) {
			if(sizeOf($terms)>0) {
				$parent= $term->parent;
				if($parent!=0){
					$locs= get_the_terms($parent, 'localita');
					if ($locs && ! is_wp_error($terms)){
						foreach ($locs as $loc) {
							if(sizeOf($locs)>0) {
								$localita= $loc->localita;
								echo $localita;
							}
						}
					}
				}else{
				 	$locality = $term->name;
					echo $locality;
				}
			}
			else{
				echo "";
			}
		}
	}
}

//create a checkbox for structure selection
function generateSelectionDiv(){ ?>
	<div class="select-checkbox">
		Seleziona <input type="checkbox" name="<?php the_ID() ?>" value="<?php the_ID() ?>">
	</div> <?php
}

//display link to structure page
function HomeElementFooter($tipoElemento){
	switch ($tipoElemento) {
		case 'struttura':
			?>
			<a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>" rel="bookmark">scheda struttura</a>
			<?php
			break;
		case 'partner':
			?>
			<a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>" rel="bookmark">scheda azienda</a>
			<?php
			break;
		default:
			break;
	}
}

?>