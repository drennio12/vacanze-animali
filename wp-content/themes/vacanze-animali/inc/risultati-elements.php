<?php
require_once('utility.php');

function ResultLoop(){
	global $categories;
	global $localities;
	global $trattamenti;
	global $services;

	if ( ( (isset($localities)) && (isset($services)) ) || ( (isset($trattamenti)) && (isset($services)) ) || ( (isset($localities)) && (isset($trattamenti)) ) || ( (isset($localities)) && (isset($services)) && (isset($trattamenti)) ) ) {
		$tax_query = array('relation' => 'AND');
    }

	$args = array(
		'post_type' => array('strutture'),
		'post_status' => 'publish'
	);

	if (isset($categories)){
		$args['category__in'] = $categories;
	}

	if (isset($localities)){
		$tax_query[] = array(
			'taxonomy' => 'localita',
			'field' => 'term_id',
			'include_children' => false,
			'terms' => $localities
		);
	}

	if (isset($trattamenti)){
        $tax_query[] = array(
            'taxonomy' => 'trattamenti',
            'field' => 'term_id',
            'include_children' => false,
            'terms' => $trattamenti
        );
    }

    if (isset($services)){
		$tax_query[] = array(
			'taxonomy' => 'servizi',
			'field' => 'term_id',
			'include_children' => false,
			'terms' => $services
		);
	}

	if ( (isset($localities)) || (isset($services)) || (isset($trattamenti)) ) {
		$args['tax_query'] = $tax_query;
	}

	$loop = new WP_Query($args);

	if ($loop->have_posts()){
		$i=1;
		while ($loop->have_posts()) : $loop->the_post();
			?>
				<article id="<?php echo "post-"; echo get_the_ID(); ?>" class="tc-grid col-xs-4 <?php echo "post-"; echo get_the_ID(); ?> has-post-thumbnail">
					<header class="home-entry-header">
						<span class="tc-grid-post">
							<div class="search-category-image">
								<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
									<?php PrintPostThumbnail('medium'); ?>
								</a>
							</div>
						</span>
						<h2 class="entry-title">
							<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
								<?php the_title(); ?>
							</a>
						</h2>
					</header>
					<div class="search-entry-content">
						<?php
							//generateCategoryDiv();
							generateLocalityDiv();
							 ?>
							<div class="search-excerpt">
								<?php echo get_the_excerpt(); ?>
							</div>
							<div class="link-scheda">
								<?php get_permalink(get_the_ID()) ?>
							</div>
							<?php generateSelectionDiv(); ?>
					</div>
					<footer class="entry-footer">
						<?php HomeElementFooter('struttura'); ?>
					</footer><!-- .entry-footer -->				
				</article>
			<?php 
		endwhile;

		$nav= get_the_posts_pagination();
		if($nav){ ?>
			<div class="lnk">
				<?php #the_posts_pagination(array('mid_size'  => 2 )); ?>
			</div><br><hr>
		<?php
		}

		$big = 999999999; // need an unlikely integer
		$pagLinkz= paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $loop->max_num_pages,
				'before_page_number' => '<span class="screen-reader-text">Qualcosa</span>'
		) );
		if($pagLinkz){
			echo '<div class="lnk">';
				echo $pagLinkz;
			echo '</div><br><hr>'; 
		}
	}; ##end if have posts
}

?>
