<?php
/**
 * The template for displaying homne page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vacanze_con_animali
 */
get_header();
include ('inc/home-elements.php');
include ('inc/utility.php');
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container">
				<!--  .article-container -->
				<div id="home">
					<!--<a href="#myModal" role="button" class="btn btn-large btn-primary col-md-2" data-toggle="modal">PROVA MODAL</a>-->
					<div id="myModal" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">Compila il modulo per inviare la richiesta</h4>
								</div>
								<div class="modal-body">
									<?php echo do_shortcode('[contact-form-7 id="88" title="Registra la tua struttura"]'); ?>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default btnsubmit" data-dismiss="modal">Chiudi</button>
								</div>
							</div>
						</div>
					</div>
					<?php
						$mappa = '<iframe src="https://www.google.com/maps/d/embed?mid=16UlzOGMxqY6mV6C_2bS88Twv3jowCVGF" width="640" height="480"></iframe>';
						$args = array(
							'post_type' => 'page',
							'pagename' => 'home'
							);
						$key= "Iframe mappa";
						$single = "false";
						$loop = new WP_Query( $args );
						$mappa1 =	get_post_meta(get_the_ID(), $key, $single);
							while ($loop->have_posts() ) : $loop->the_post();?>
						<!--Mappa e testo-->
						<div id="centro" class="col-md-12 col-xs-12">
							<div class="col-md-6 col-xs-12">
								<?php echo get_post_meta(get_the_ID(), $key, $single);?>
							</div>
							<div class="col-md-6 col-xs-12">
								<p>
									<?php 	echo get_the_content();?>
								</p>
							</div>
							<?php	endwhile; // End of the loop.
					?>
						</div>
						<!--Articoli custom taxonomies -->
						<div id="custom articles" class="col-xs-12 col-md-12">
							<!--INIZIO CODICE POST TYPE STRUTTURE -->
							<?php
							$args1 = array(
								'post_type' => 'strutture',
								'posts_per_page' => '2',
							);
							$loop1 = new WP_Query( $args1 );
							$obj = get_post_type_object('strutture');
							$post_type_link = get_post_type_archive_link( $post_type );
							?>
								<div class="categorycontainer col-md-6">
									<div class="titlecontainer col-md-12">
										<a href="<?php echo $post_type_link; ?>" class="categorylink">
											<img class="marker" width="32px" height="32px" src="http://localhost/vacanze-animali/wp-content/uploads/<?php echo $obj->name; ?>-marker.png">
											<h2 class="markertitle">
												<?php echo $obj->name;?>
											</h2>
										</a>
									</div>
									<div class="col-md-12">
										<?php	while ($loop1->have_posts() ) : $loop1->the_post();	?>
											<div class="article col-xs-6 col-md-6">
												<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
													<header class="home-article-header">
														<a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>" rel="bookmark">
															<?php the_post_thumbnail('thumbnail') ?>
														</a>
														<?php  the_title('<h3 id="article-title" class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h3>' );?>
													</header>
												</article>
											</div>
											<!--Single article -->
											<?php endwhile;?>
									</div>
									<!--Title container -->
								</div>
								<!--Category container-->
								<!--FINE CODICE PER POST TYPE STRUTTURE -->
								<?php 
							$categories = get_categories('taxonomy=categorie-partner&post_type=partner');
							foreach ($categories as $category){ //Ciclo per prendere i dati dalle categories
							$getlink = get_category_link($category->cat_ID);
							$catID = get_category($category->cat_ID);
							$terms = get_the_terms( get_the_ID(), 'categorie-partner' ); 						
							$category_links = array();
							$args = array(
								'post_type' => 'partner',
								'posts_per_page' => '2',
								'tax_query' => array(
								array(
								'taxonomy' => 'categorie-partner',
								'field' => 'slug',
								'terms' => "$category->slug",
								)
								));
							$loop = new WP_Query( $args );
							$tax = get_query_var( 'taxonomy' );
							?>
								<div class="categorycontainer col-md-6 col-xs-12">
									<div class="titlecontainer col-md-12 col-xs-12">
										<a href="<?php echo $getlink; ?>" class="categorylink">
											<?php// DA AGGIUSTARE USANDO LA FUNZIONE IN ULTILITY.PHP CategoryImage(); ?>
											<img class="marker" width="32px" height="32px" src="http://localhost/vacanze-animali/wp-content/uploads/<?php echo $category->slug; ?>-marker.png">
											<h2 class="markertitle">

												<?php echo $tax; echo $category->name;?>
											</h2>
										</a>
									</div>
									<div class="col-md-12">
										<?php	while ($loop->have_posts() ) : $loop->the_post();	?>
											<div class="article col-xs-6 col-md-6">
												<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
													<header class="home-article-header">
														<a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>" rel="bookmark">
															<?php the_post_thumbnail('thumbnail') ?>
														</a>
														<?php the_title('<h3 id="article-title" class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h3>' );?>
													</header>
												</article>
											</div>
											<!--Single article -->
											<?php endwhile;?>
											<!--COMMENTATO BOTTONE "VAI" PER FUNZIONALITA' GIA' IMPLEMENTATE NEL TITOLODELL'ARTICOLO 
									<a class="link col-md-2" href="<?php //echo $getlink; ?>">
									<div class="btntest">
										<h1>
											VAI
										</h1>
									</div>
								</a>-->
									</div>
									<!--Title container -->
								</div>
								<!--Category container-->
								<?php } ?>
						</div>
						<!--CUSTOM ARTICLES-->
				</div>
				<!--HOME-->
			</div>
			<!-- .container -->
		</main>
		<!-- #main -->
	</div>
	<!-- #primary -->

	<?php
get_sidebar();
get_footer();