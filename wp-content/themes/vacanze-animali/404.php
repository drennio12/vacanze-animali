<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package vacanze_con_animali
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container">
				<section class="error-404 not-found">
					<header class="page-header">
						<h1 class="page-title">Pagina non trovata</h1>
					</header><!-- .page-header -->

					<div class="page-content">
						<p>La pagina che hai indicato non esiste, utilizza il menu per selezionare una pagina del sito</p>

					</div><!-- .page-content -->
				</section><!-- .error-404 -->
			</div><!-- .container -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
