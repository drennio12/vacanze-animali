<?php
/**
 * The template for search result page
 *
 * @package vacanze_con_animali
 */
get_header();

include_once('inc/utility.php');
include_once('inc/risultati-elements.php');

$url=get_theme_root_uri();
$site_url = network_site_url( '/' );

if (isset($_POST['category'])) $categories =  $_POST['category'];
if (isset($_POST['localita'])) $localities =  $_POST['localita'];
if (isset($_POST['trattamenti'])) $trattamenti =  $_POST['trattamenti'];
if (isset($_POST['servizi'])) $services =  $_POST['servizi'];
global $categories;
global $localities;
global $trattamenti;
global $services;
?>

<div id="main-wrapper" class="">
    <div class="container" role="main">
        <h2 class="structures">Risultati della ricerca</h2>
        <div id="content" class="article-container">
            <?php 
                ResultLoop();
            ?>
        </div><!--.article-container -->
        <div class="back col-xs-12">
            <div id="ricerca-link" style="text-align: center;">
                <a href="<?php echo $site_url ?>ricerca-avanzata" class="btn btn-inverse">Torna alla Ricerca</a>
            </div>
        </div>
    </div><!-- .container role: main -->
</div><!--#main-wrapper"-->

<?php
get_sidebar();
get_footer();
