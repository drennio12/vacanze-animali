<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vacanze_con_animali
 */

get_header();

include ('inc/home-elements.php');
include ('inc/utility.php');
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container">
                <h2 class="partners">Aziende che offrono servizi per animali</h2>
				<?php
                    //partners loop
                    $args = array(
                        'post_type' => 'partner',
                        'tax_query' => array(
                            array(
                                'taxonomy' => $taxonomy,
                                'field'    => 'slug',
                                'terms'    => $term
                            )
                        )
                    );
                    $loop = new WP_Query( $args );
                ?>
                <div class="article-container col-xs-12">
                <?php
                    while ($loop->have_posts() ) : $loop->the_post();
                        ArchiveArticles();
                    endwhile; // End of the loop.
                ?>
                </div><!--  .article-container -->
                <div id="home">
                    <?php
                        $args = array(
                            'post_type' => 'page',
                            'pagename' => 'home'
                            );

                        $loop = new WP_Query( $args );
            				//while ($loop->have_posts() ) : $loop->the_post();
                              //  echo get_the_content();
                            //endwhile; // End of the loop.
                    ?>
                </div>

			</div><!-- .container -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
